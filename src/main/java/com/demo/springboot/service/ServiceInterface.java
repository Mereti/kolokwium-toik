package com.demo.springboot.service;

import com.demo.springboot.dto.PozycjaDto;
import com.demo.springboot.dto.StatkiDto;

public interface ServiceInterface {

    Boolean sprawdzTablice(Integer liczba);

}
