package com.demo.springboot.rest;


import com.demo.springboot.service.ServiceImplementation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api")

public class DocumentApiController {

    private static final Logger LOGGER = LoggerFactory.getLogger(DocumentApiController.class);



    private ServiceImplementation serviceImplementation;
    //StatkiDto statkiDto;


    public DocumentApiController(  ServiceImplementation serviceImplementation) {
        this.serviceImplementation = serviceImplementation;


    }

    @PostMapping(value="/shot")// @RequestParam (value = "position")
    public ResponseEntity<String> myPosition(@RequestParam  Integer position ){

        LOGGER.info("position = {}",position);

        if(position > 8){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } else if(serviceImplementation.sprawdzTablice(position)){
            // Sprawdzic jaki obiket nalezy dodac
            return new ResponseEntity<>(HttpStatus.OK); // jesli trafi
        }else if(serviceImplementation.sprawdzTablice(position)== false){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND); //nie trafi
        }else return new ResponseEntity<>(HttpStatus.BAD_REQUEST); // inne bledy

        }

    }


