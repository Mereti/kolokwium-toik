package com.demo.springboot.dto;

import java.util.Arrays;

public class StatkiDto {

    private String [] tablicaSatkow;

    public StatkiDto(String[] tablicaSatkow) {
        this.tablicaSatkow = tablicaSatkow;
    }
    public StatkiDto() {

    }

    public String[] getTablicaSatkow() {
        return tablicaSatkow;
    }

    public void setTablicaSatkow(String[] tablicaSatkow) {
        System.out.println(tablicaSatkow);
        this.tablicaSatkow = tablicaSatkow;
    }

    @Override
    public String toString() {
        return "StatkiDto{" +
                "tablicaSatkow=" + Arrays.toString(tablicaSatkow) +
                '}';
    }
}
