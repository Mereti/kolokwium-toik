package com.demo.springboot.dto;

public class PozycjaDto {

    private Integer x;


    public PozycjaDto(Integer x) {
        this.x = x;
    }

    public PozycjaDto() {

    }

    public Integer getX() {
        return x;
    }

    public void setX(Integer x) {
        this.x = x;
    }

    @Override
    public String toString() {
        return "PozycjaDto{" +
                "x=" + x +
                '}';
    }
}
